import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static org.junit.Assert.*
import javax.persistence.StoredProcedureParameter as StoredProcedureParameter
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.junit.Test as Test
import org.openqa.selenium.By as By

WebUI.click(findTestObject('Mini Cart WishList/link_MiniCart'))

WebDriver driver = DriverFactory.getWebDriver()

def elecountx = driver.findElements(By.xpath('//div[contains(@class,\'item-list \')]')).size

//int exlfile = Integer.parseInt(findTestData('Checkout Function').getValue(2, 1))
int a = 0

int count = 1

int elecount = elecountx

float minicart_PriceTag_number

float totalprice

int text_Qty_number

int totalQty

'Verify Product name, image, size, colour, price, promo, subtotal, free express shipping text on Mini cart'
while (elecountx != a) {
    text_Qty = WebUI.getAttribute(findTestObject('Cart Landing Page/text_Qty', [('count') : count]), 'value', FailureHandling.OPTIONAL)

    text_Qty_number = Integer.parseInt(text_Qty)

    'Verify the scrolling when there are many products in mini cart'
    if (count >= 4) {
        countx = (count - 1)

        WebUI.scrollToElement(findTestObject('Mini Cart/List_Product', [('count') : countx]), 5)
    }
    
    String text_ProductPromo

    if (WebUI.verifyElementPresent(findTestObject('Cart Landing Page/label_Promotion', [('count') : count]), 1, FailureHandling.OPTIONAL) == 
    true) {
        //int textLength = WebUI.getText(findTestObject('Product Details Page/text_ProductPromotion')).length()
        //text_ProductPromo = WebUI.getText(findTestObject('Product Details Page/text_ProductPromotion')).substring(0, textLength -8)
        'Get text ProductPromotion'
        text_ProductPromo = WebUI.getText(findTestObject('Cart Landing Page/label_Promotion', [('count') : count]))
    }
    
    //text_ProductName = WebUI.getText(findTestObject('Cart Landing Page/label_ProductName', [('count') : count]))

    text_ProductColour = WebUI.getText(findTestObject('Cart Landing Page/label_Colour', [('count') : count]))

    text_ProductSize = WebUI.getText(findTestObject('Cart Landing Page/label_Size', [('count') : count]))

    WebUI.mouseOver(findTestObject('Mini Cart WishList/link_MiniCart'))

    WebUI.scrollToElement(findTestObject('Mini Cart/layout_simplebar-content', [('elecount') : elecount]), 5)

    minicart_ProductName = WebUI.getText(findTestObject('Mini Cart/label_ProductName', [('elecount') : elecount]))

    minicart_PriceTag = WebUI.getText(findTestObject('Mini Cart/label_PriceTag', [('elecount') : elecount])).substring(5)

    minicart_PriceTag_number = Float.parseFloat(minicart_PriceTag)

    minicart_Size = WebUI.getText(findTestObject('Mini Cart/label_Size', [('elecount') : elecount]))

    minicart_Colour = WebUI.getText(findTestObject('Mini Cart/label_Colour', [('elecount') : elecount]))

    minicart_Qty = WebUI.getText(findTestObject('Mini Cart/label_Qty', [('elecount') : elecount]))

    WebUI.verifyElementPresent(findTestObject('Mini Cart/lable_imageProduct', [('elecount') : elecount]), 5)

    if (WebUI.verifyElementPresent(findTestObject('Mini Cart/label_Promo', [('elecount') : elecount]), 1, FailureHandling.OPTIONAL) == 
    true) {
        minicart_ProductPromotion = WebUI.getText(findTestObject('Mini Cart/label_Promo', [('elecount') : elecount]))

        'Verify promo if have'
        WebUI.verifyEqual(minicart_ProductPromotion, text_ProductPromo, FailureHandling.STOP_ON_FAILURE)
    }
    
    'Verify Product Name is presented'
    WebUI.verifyElementPresent(findTestObject('Mini Cart/label_ProductName', [('elecount') : elecount]), 5)

    'Verify Price on each item'
    WebUI.verifyElementPresent(findTestObject('Mini Cart/label_PriceTag', [('elecount') : elecount]), 5)

    'Verify Size on each item'
    WebUI.verifyEqual(minicart_Size, text_ProductSize, FailureHandling.STOP_ON_FAILURE)

    'Verify Colour on each item'
    WebUI.verifyEqual(minicart_Colour, text_ProductColour, FailureHandling.STOP_ON_FAILURE)

    'Verify QTy on each item'
    WebUI.verifyEqual(minicart_Qty, text_Qty_number, FailureHandling.STOP_ON_FAILURE)

    totalprice = (totalprice + minicart_PriceTag_number)

    totalQty = (totalQty + text_Qty_number)

    //println(totalprice)

    a = (a + 1)

    count = (count + 1)

    elecount = (elecount - 1)
}

totalpricerounded = totalprice.round(2)

text_BAG = WebUI.getText(findTestObject('Mini Cart/label_title_number_bag')).substring(4)

text_numberOF_BAG = Integer.parseInt(text_BAG)

'Verify number of Bag on miniCart and Total of Qty should be the same'
WebUI.verifyEqual(totalQty, text_numberOF_BAG, FailureHandling.STOP_ON_FAILURE)

'move over on Minicart icon'
WebUI.mouseOver(findTestObject('Mini Cart WishList/link_MiniCart'))

minicart_Subtotal = WebUI.getText(findTestObject('Mini Cart/label_SubTotal_Price')).substring(1)

minicart_Subtotal_number = Float.parseFloat(minicart_Subtotal)

'Verify subtotal on miniCart and Total on Cart Page should be the same'
WebUI.verifyEqual(minicart_Subtotal_number, totalpricerounded, FailureHandling.STOP_ON_FAILURE)

'Verify text "FREE EXPRESS SHIPPING" should be displayed when total over $150'
if (minicart_Subtotal_number >= 150) {
    WebUI.verifyElementText(findTestObject('Mini Cart/label_SubTotal_Text'), 'SUBTOTAL (FREE EXPRESS SHIPPING)')
}

'Verify button <Go to bag & Checkout>'
WebUI.verifyElementClickable(findTestObject('Mini Cart/button_GoToBagAndCheckOut'))

urlcurrent = WebUI.getUrl()

'Verify URL on click'
WebUI.verifyElementAttributeValue(findTestObject('Mini Cart/button_GoToBagAndCheckOut'), 'href', urlcurrent, 2)

