import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

'Verify Country label\r\n'
WebUI.verifyElementPresent(findTestObject('Header/label_CountrySelector'), 3)

'Verify site option- bardot and bardot junior\r\n'
WebUI.verifyElementPresent(findTestObject('Header/button_SiteOption_junior'), 3)

'Verify actived effect on site branch'
WebUI.verifyElementPresent(findTestObject('Header/button_SiteOption - actived'), 3)

'Verify 2 site branch buttons are clickable'
WebUI.verifyElementClickable(findTestObject('Header/button_SiteOption - actived'))

'Verify header\'s campaign text\r\n'
WebUI.verifyElementVisible(findTestObject('Header/label_HeaderPromotion_Text'), FailureHandling.STOP_ON_FAILURE)

'Go to configure page\r\n'
WebUI.verifyElementClickable(findTestObject('Header/label_HeaderPromotion_Text'))

'Verify login button\r\n'
WebUI.verifyElementPresent(findTestObject('Header/link_Login'), 3)

'Get current CSS value\r\n'
css_bg_default = WebUI.getCSSValue(findTestObject('Header/link_Login'), 'background-color')

'mouse over'
WebUI.mouseOver(findTestObject('Header/link_Login'))

'Get CSS value after mouse over\r\n'
css_bg_hover = WebUI.getCSSValue(findTestObject('Header/link_Login'), 'background-color')

'Verify hovering effect on Login icon'
WebUI.verifyEqual(css_bg_hover, 'rgba(0, 0, 0, 1)')

'Verify wishlist button'
WebUI.verifyElementPresent(findTestObject('Header/link_WishList'), 3)

'Get current CSS value\r\n'
css_bg_default = WebUI.getCSSValue(findTestObject('Header/link_WishList'), 'background-color')

'mouse over'
WebUI.mouseOver(findTestObject('Header/link_WishList'))

'Get CSS value after mouse over\r\n'
css_bg_hover = WebUI.getCSSValue(findTestObject('Header/link_WishList'), 'background-color')

'Verify hovering effect on Wish List icon'
WebUI.verifyEqual(css_bg_hover, 'rgba(0, 0, 0, 1)')

'Verify mini cart icon'
WebUI.verifyElementPresent(findTestObject('Header/link_MiniCart'), 3)

'Get current CSS value\r\n'
css_bg_default = WebUI.getCSSValue(findTestObject('Header/link_MiniCart'), 'background-color')

'mouse over'
WebUI.mouseOver(findTestObject('Header/link_MiniCart'))

'Get CSS value after mouse over\r\n'
css_bg_hover = WebUI.getCSSValue(findTestObject('Header/link_MiniCart'), 'background-color')

'Verify hovering effect on Mini cart icon'
WebUI.verifyEqual(css_bg_hover, 'rgba(0, 0, 0, 1)')

'Mouse over to login\r\n'
WebUI.mouseOver(findTestObject('Header/link_Login'))

'Verify Login button is clickable'
WebUI.verifyElementClickable(findTestObject('Header/button_Login'))

'Verify Login button is clickable'
WebUI.verifyElementClickable(findTestObject('Header/button_Register'))

WebUI.callTestCase(findTestCase('Common Test Cases/Logged As Regitered'), [:], FailureHandling.STOP_ON_FAILURE)

'Mouse over to user profile'
WebUI.mouseOver(findTestObject('Header/UserProfile'))

'Click on My Account'
WebUI.click(findTestObject('Header/button_MyAccount'))

'Verify user on My account landing page'
WebUI.verifyElementPresent(findTestObject('Header/breadcrumb_Account_Landing_Page'), 3)

'Mouse over to user profile'
WebUI.mouseOver(findTestObject('Header/UserProfile'))

'click on MyOrder'
WebUI.click(findTestObject('Header/button_MyOrders'))

'Verify user on My Order landing page'
WebUI.verifyElementPresent(findTestObject('Header/breadcrumb_Orders_Landing_Page'), 3)

'Click on wishlist\r\n'
WebUI.click(findTestObject('Header/link_WishList'))

'Verify user on My Wishlist landing page'
WebUI.verifyElementPresent(findTestObject('Header/breadcrumb_Wishlist_Landing_Page'), 3)

'Click on ini cart'
WebUI.click(findTestObject('Header/link_Cart'))

'Verify user on Cart Landing page'
WebUI.verifyElementPresent(findTestObject('Header/empty_Cart_Landing_Page'), 3)

'Mouse over to user profile'
WebUI.mouseOver(findTestObject('Header/UserProfile'))

'Click on logout'
WebUI.click(findTestObject('Header/button_Logout'))

'Verify user on Login page page'
WebUI.verifyElementPresent(findTestObject('Header/Login_Account_Page'), 3)

'Verify search icon'
WebUI.verifyElementPresent(findTestObject('Header/icon_Search'), 3)

'Click on search icon'
WebUI.click(findTestObject('Header/icon_Search'))

'Verify Search suggession form'
WebUI.verifyElementPresent(findTestObject('Header/Search_suggestion_form'), 3)

'close the search form'
WebUI.click(findTestObject('Header/close_Search_suggestion'))

'Click on home page'
WebUI.click(findTestObject('Common Test Cases/Primary_Logo'))

'Scroll down'
WebUI.scrollToPosition(0, 150)

'Get current CSS value\r\n'
height = WebUI.getCSSValue(findTestObject('Common Test Cases/Primary_Logo'), 'height')

'if User on Junior'
if (WebUI.verifyElementPresent(findTestObject('Header/button_SiteOption_junior'), 3, FailureHandling.OPTIONAL)) {
    'Verify logo-height = 32px'
    WebUI.verifyEqual(height, '32px')
} else {
    'Verify logo-height = 30px'
    WebUI.verifyEqual(height, '30px')
}

