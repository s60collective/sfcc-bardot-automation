import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.thoughtworks.selenium.webdriven.commands.Highlight as Highlight
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.stringtemplate.v4.compiler.CodeGenerator.region_return as region_return
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

if (WebUI.verifyElementPresent(findTestObject('Header/button_SiteOption_junior'), 3, FailureHandling.OPTIONAL)) {
    'if User on Junior'
    int jncol = 1

    int jncolexl = 2

    while (jncol <= 8) {
        int jnrowexl = 31

        int jnrow = 1

        int jnrow2 = 1

        int column = 1

        'Mouse over on 1st menu'
        WebUI.mouseOver(findTestObject('Mega Menu/list_Mega_category_lv1 - Junior', [('jncol') : jncol]))

        'Verify Banner image is presented and match excel file'
        WebUI.verifyElementPresent(findTestObject('Mega Menu/Image_banner - Junior', [('data') : findTestData('Mega Menu').getValue(
                        jncolexl, 59)]), 3)

        'Verify CTA button and URL should be the same on excel file'
        WebUI.verifyElementPresent(findTestObject('Mega Menu/Banner_button_URL - Junior', [('dataa') : findTestData('Mega Menu').getValue(
                        jncolexl, 60)]), 3)

        text1 = WebUI.getText(findTestObject('Mega Menu/list_Mega_category_lv1 - Junior', [('jncol') : jncol]))

        'Compare all names on menu with Excel file'
        WebUI.verifyEqual(findTestData('Mega Menu').getValue(jncolexl, jnrowexl), text1)

        while (column < 4) {
            if (WebUI.verifyElementPresent(findTestObject('Mega Menu/list_Sub_categories_column - Junior', [('column') : column
                        , ('jncol') : jncol]), 1, FailureHandling.STOP_ON_FAILURE) && (WebUI.getText(findTestObject('Mega Menu/list_Sub_categories_column - Junior', 
                    [('column') : column, ('jncol') : jncol])) != '')) {
                jnrowexl = (jnrowexl + 1)

                textrow1 = WebUI.getText(findTestObject('Mega Menu/list_Sub_categories_column - Junior', [('column') : column
                            , ('jncol') : jncol]))

                WebUI.verifyElementClickable(findTestObject('Mega Menu/list_Sub_categories_column - Junior', [('column') : column
                            , ('jncol') : jncol]))

                WebUI.verifyEqual(textrow1, findTestData('Mega Menu').getValue(jncolexl, jnrowexl))

                while (jnrow2 != 9) {
                    textrow2 = WebUI.getText(findTestObject('Mega Menu/list_Sub_categories- Junior', [('jncol') : jncol, ('jnrow2') : jnrow2
                                , ('column') : column]))

                    jnrowexl = (jnrowexl + 1)

                    WebUI.verifyEqual(textrow2, findTestData('Mega Menu').getValue(jncolexl, jnrowexl))

                    jnrow2 = (jnrow2 + 1)

                    if (WebUI.verifyElementNotPresent(findTestObject('Mega Menu/list_Sub_categories- Junior', [('jncol') : jncol
                                , ('jnrow2') : jnrow2, ('column') : column]), 1, FailureHandling.OPTIONAL) || (WebUI.getText(
                        findTestObject('Mega Menu/list_Sub_categories- Junior', [('jncol') : jncol, ('jnrow2') : jnrow2, ('column') : column])) == 
                    '')) {
                        jnrow2 = 1

                        break
                    }
                }
                
                jnrowexl = 32
            } else {
                break
            }
            
            column = (column + 1)

            jnrowexl = (jnrowexl + 8)

            if (column == 3) {
                jnrowexl = 49
            }
        }
        
        jncol = (jncol + 1)

        jncolexl = (jncolexl + 1)
    }
} else {
    'Verify Mega Menu is presented '
    WebUI.verifyElementPresent(findTestObject('Mega Menu/list_MegaMenu'), 3)

    int col = 1

    int colexl = 2

    while (col <= 7) {
        int rowexl = 5

        int row = 1

        int row2 = 1

        'Mouse over on 1st menu'
        WebUI.mouseOver(findTestObject('Mega Menu/list_Mega_category_lv1', [('col') : col]))

        'Verify Banner image is presented and match excel file'
        WebUI.verifyElementPresent(findTestObject('Mega Menu/Image_banner', [('data') : findTestData('Mega Menu').getValue(
                        colexl, 21)]), 3)

        'Verify CTA button and URL should be the same on excel file'
        WebUI.verifyElementPresent(findTestObject('Mega Menu/Banner_button_URL', [('dataa') : findTestData('Mega Menu').getValue(
                        colexl, 22)]), 3)

        text2 = WebUI.getText(findTestObject('Mega Menu/list_Sub_categories', [('col') : col, ('row') : row]))

        text1 = WebUI.getText(findTestObject('Mega Menu/list_Mega_category_lv1', [('col') : col]))

        'Compare all names on menu with Excel file'
        WebUI.verifyEqual(findTestData('Mega Menu').getValue(colexl, rowexl), text1)

        'find all mega nemu and compare to Excel file'
        while (((row == 7) || (WebUI.getText(findTestObject('Mega Menu/list_Sub_categories', [('col') : col, ('row') : row])) != 
        '')) && (row2 != 7)) {
            rowexl = (rowexl + 1)

            text2 = WebUI.getText(findTestObject('Mega Menu/list_Sub_categories', [('col') : col, ('row') : row]))

            WebUI.verifyEqual(findTestData('Mega Menu').getValue(colexl, rowexl), text2, FailureHandling.STOP_ON_FAILURE)

            row = (row + 1)

            if (WebUI.verifyElementNotPresent(findTestObject('Mega Menu/list_Sub_categories', [('col') : col, ('row') : row]), 
                1, FailureHandling.OPTIONAL)) {
                break
            }
            
            if (row == 7) {
                while (row2 != 7) {
                    text3 = WebUI.getText(findTestObject('Mega Menu/list_row2', [('row2') : row2, ('col') : col]))

                    rowexl = (rowexl + 1)

                    WebUI.verifyEqual(findTestData('Mega Menu').getValue(colexl, rowexl), text3, FailureHandling.STOP_ON_FAILURE)

                    row2 = (row2 + 1)

                    if (WebUI.verifyElementNotPresent(findTestObject('Mega Menu/list_row2', [('row2') : row2, ('col') : col]), 
                        1, FailureHandling.OPTIONAL)) {
                        row2 = 7
                    }
                }
            }
        }
        
        col = (col + 1)

        colexl = (colexl + 1)
    }
}

