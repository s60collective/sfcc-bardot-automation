import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.junit.Test as Test
import org.openqa.selenium.By as By

WebUI.click(findTestObject('Header/link_WishList'))

WebDriver driver = DriverFactory.getWebDriver()

def elec = driver.findElements(By.xpath('//form[contains(@name,"dwfrm_wishlist_items_i")]')).size

println(elec)

num = 0

while (num != elec) {
    WebUI.click(findTestObject('Mini Cart WishList/button_Remove'))

    WebUI.waitForJQueryLoad(5)

    //WebUI.scrollToElement(findTestObject('Mini Cart WishList/button_Remove', [('num') : num]), 2)
    num = (num + 1)
	println(num)
	
}

