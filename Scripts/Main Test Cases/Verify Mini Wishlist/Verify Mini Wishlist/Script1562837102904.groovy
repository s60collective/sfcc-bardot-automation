import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.junit.Test as Test
import org.openqa.selenium.By as By

WebUI.callTestCase(findTestCase('Common Test Cases/Logged As Regitered'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Main Test Cases/Verify Mini Wishlist/Remove All Current My WishList'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Main Test Cases/Verify Mini Wishlist/Add Specific Product Into WishList'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Mini Cart WishList/link_MiniCartWishList'))

WebUI.mouseOver(findTestObject('Mini Cart WishList/link_MiniCartWishList'))

WebDriver driver = DriverFactory.getWebDriver()

def elec = driver.findElements(By.xpath('//form[contains(@name,"dwfrm_wishlist_items_i")]')).size

println(elec)

num = 0

int elecount = num

String form

while (num != elec) {
    form = WebUI.getAttribute(findTestObject('My Account - Wishlist/List_All_Items', [('elecount') : elecount]), 'data-pid')

    WebUI.scrollToElement(findTestObject('Mini Cart WishList/text_ProductName', [('form') : form]), 2)

    'show Product Name'
    get_ProductName = WebUI.getText(findTestObject('Mini Cart WishList/text_ProductName', [('form') : form]))

    'Verify each mini wishlist should be presented'
    WebUI.verifyElementPresent(findTestObject('Mini Cart WishList/img_MiniCartProduct', [('form') : form]), 1)

    'Verify text ProductPromotion if have'
    if (WebUI.verifyElementPresent(findTestObject('Mini Cart WishList/text_Promo', [('form') : form]), 1, FailureHandling.OPTIONAL) == 
    true) {
        'show Promo Name'
        get_Promo = WebUI.getText(findTestObject('Mini Cart WishList/text_Promo', [('form') : form]))

        CustomKeywords.'custom.HighlightElement.run'(findTestObject('Mini Cart WishList/text_Promo', [('form') : form]))
    }
    
    'Verify price on each product'
    WebUI.verifyElementPresent(findTestObject('Mini Cart WishList/text_Price', [('form') : form]), 1)

    'show Price'
    get_Price = WebUI.getText(findTestObject('Mini Cart WishList/text_Price', [('form') : form]))

    'Verify Size on each product'
    WebUI.verifyElementPresent(findTestObject('Mini Cart WishList/text_Size', [('form') : form]), 1)

    'show Size'
    get_Size = WebUI.getText(findTestObject('Mini Cart WishList/text_Size', [('form') : form]))

    'Verify Color on each product'
    WebUI.verifyElementPresent(findTestObject('Mini Cart WishList/text_Colour', [('form') : form]), 1)

    'show Color'
    get_Color = WebUI.getText(findTestObject('Mini Cart WishList/text_Colour', [('form') : form]))

    'Verify Qty on each product'
    WebUI.verifyElementPresent(findTestObject('Mini Cart WishList/text_Qty', [('form') : form]), 1)

    'show Qty'
    get_Qty = WebUI.getText(findTestObject('Mini Cart WishList/text_Qty', [('form') : form]))

    CustomKeywords.'custom.HighlightElement.run'(findTestObject('Mini Cart WishList/text_ProductName', [('form') : form]))

    CustomKeywords.'custom.HighlightElement.run'(findTestObject('Mini Cart WishList/text_Price', [('form') : form]))

    CustomKeywords.'custom.HighlightElement.run'(findTestObject('Mini Cart WishList/text_Size', [('form') : form]))

    CustomKeywords.'custom.HighlightElement.run'(findTestObject('Mini Cart WishList/text_Qty', [('form') : form]))

    CustomKeywords.'custom.HighlightElement.run'(findTestObject('Mini Cart WishList/text_Colour', [('form') : form]))

    num = (num + 1)

    elecount = (elecount + 1)

    println(elecount)

    println(form)
}

