import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

'Click on Login button from menu'
WebUI.click(findTestObject('Common Test Cases/span_Login'))

'Verify "Login Button" should be disabled'
WebUI.waitForElementAttributeValue(findTestObject('Login Page/button_Login'), 'class', 'button active disabled', 
    3)

'Enter email address'
WebUI.setText(findTestObject('Login Page/input_login_username'), findTestData('Login Page').getValue(2, 7))

'Enter password'
WebUI.setText(findTestObject('Login Page/input_login_password'), findTestData('Login Page').getValue(2, 8))

'click Login button'
WebUI.click(findTestObject('Login Page/button_Login'))

'verify text "Welcome,..." should not be displayed'
WebUI.verifyElementNotPresent(findTestObject('Login Page/span_Welcome user'), 5)

'Verify Error message should be displayed as Excel file\r\n'
WebUI.verifyElementText(findTestObject('Login Page/span_error_Email'), findTestData('Login Page').getValue(3, 7))

