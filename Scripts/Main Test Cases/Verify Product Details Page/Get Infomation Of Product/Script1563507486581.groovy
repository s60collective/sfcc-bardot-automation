import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

text_ProductName = WebUI.getText(findTestObject('Product Details Page/text_ProductName'), FailureHandling.OPTIONAL)

text_ProductPrice = WebUI.getAttribute(findTestObject('Product Details Page/text_ProductPrice'), 'value', FailureHandling.OPTIONAL)

'text_ProductStandardPrice'
if (WebUI.verifyElementPresent(findTestObject('Product Details Page/text_ProductStandardPrice'), 1, FailureHandling.OPTIONAL) == 
true) {
    text_ProductStandardPrice = WebUI.getAttribute(findTestObject('Product Details Page/text_ProductStandardPrice'), 'value').substring(
        5)
}

if (WebUI.verifyElementPresent(findTestObject('Product Details Page/text_ProductPromotion'), 1, FailureHandling.OPTIONAL) == 
true) {
    int textLength = WebUI.getText(findTestObject('Product Details Page/text_ProductPromotion')).length()

    textLengthc = WebUI.getText(findTestObject('Product Details Page/text_ProductPromotion'))

    println(textLengthc)

    println(textLength)

    'text_ProductPromotion'
    text_ProductPromotion = WebUI.getText(findTestObject('Product Details Page/text_ProductPromotion')).substring(0, textLength - 
        7)

    println(text_ProductPromotion)
}

text_ProductColour = WebUI.getText(findTestObject('Product Details Page/text_ProductColour'), FailureHandling.OPTIONAL)

text_ProductSize = WebUI.getText(findTestObject('Product Details Page/text_ProductSize'), FailureHandling.OPTIONAL)

count = 2

text_Qty = WebUI.getAttribute(findTestObject('Cart Landing Page/text_Qty', [('count') : 1]), 'value', FailureHandling.OPTIONAL)

int text_Qty_number = Integer.parseInt(text_Qty)

minicart_ProductName = WebUI.getText(findTestObject('Mini Cart/label_ProductName', [('count') : count]))

'minicart_PriceTag'
minicart_PriceTag = WebUI.getText(findTestObject('Mini Cart/label_PriceTag', [('count') : count])).substring(5)

float minicart_PriceTag_number = Float.parseFloat(minicart_PriceTag)

float actual_PriceTag_perqty = minicart_PriceTag_number / text_Qty_number

minicart_Size = WebUI.getText(findTestObject('Mini Cart/label_Size', [('count') : count]))

minicart_Colour = WebUI.getText(findTestObject('Mini Cart/label_Colour', [('count') : count]))

minicart_Qty = WebUI.getText(findTestObject('Mini Cart/label_Qty', [('count') : count]))

WebUI.scrollToElement(findTestObject('Mini Cart/layout_simplebar-content'), 5)

count = 1

WebUI.scrollToElement(findTestObject('Mini Cart/List_Product', [('count') : count]), 5)

elecount = 2

WebUI.verifyElementPresent(findTestObject('Mini Cart/lable_imageProduct', [('elecount') : elecount]), 5)
if (WebUI.verifyElementPresent(findTestObject('Mini Cart/label_Promo',[('elecount') : elecount]), 1, FailureHandling.OPTIONAL) ==
	true) {

		'text_ProductPromotion'
		minicart_ProductPromotion = WebUI.getText(findTestObject('Mini Cart/label_Promo',[('elecount') : elecount]))
		
		
	}
	count = 1
	text_ProductPrice = WebUI.getText(findTestObject('Cart Landing Page/label_SalePrice',[('count') : count]))
	println text_ProductPrice