import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.junit.Test as Test
import org.openqa.selenium.By as By

WebUI.callTestCase(findTestCase('Common Test Cases/001-Open Home Page Bardot Site'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Common Test Cases/Add Product Into Your Cart/Add Specific Product Into Cart'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Mini Cart WishList/link_MiniCart'))

WebDriver driver = DriverFactory.getWebDriver()

def elecountx = driver.findElements(By.xpath('//div[contains(@class,\'item-list \')]')).size

//int exlfile = Integer.parseInt(findTestData('Checkout Function').getValue(2, 1))
int a = 0

int count = 1

int text_Qty_number

int totalQty

while (elecountx != a) {
    text_Qty = WebUI.getAttribute(findTestObject('Cart Landing Page/text_Qty', [('count') : count]), 'value', FailureHandling.OPTIONAL)

    text_Qty_number = Integer.parseInt(text_Qty)

    if (WebUI.verifyElementPresent(findTestObject('Cart Landing Page/label_Promotion', [('count') : count]), 1, FailureHandling.OPTIONAL) == 
    true) {
        'Verify ProductPromotion is presented'
        WebUI.verifyElementPresent(findTestObject('Cart Landing Page/label_Promotion', [('count') : count]), 1)
		
    }
    
    'Verify Product Name is presented'
    WebUI.verifyElementPresent(findTestObject('Cart Landing Page/label_ProductName', [('count') : count]), 1)

    'Verify Colour is presented'
    WebUI.verifyElementPresent(findTestObject('Cart Landing Page/label_Colour', [('count') : count]), 1)

    'Verify Size is presented'
    WebUI.verifyElementPresent(findTestObject('Cart Landing Page/label_Size', [('count') : count]), 1)
	'Click on Edit link'
    WebUI.click(findTestObject('Cart Landing Page/button_Edit', [('count') : count]))

    WebUI.waitForJQueryLoad(5)
	'Click close quickview'
    WebUI.click(findTestObject('Cart Landing Page/button_Close_Quickview'))

    a = (a + 1)

    count = (count + 1)
}

