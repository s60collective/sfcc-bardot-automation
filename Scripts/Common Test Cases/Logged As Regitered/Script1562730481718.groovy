import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Click on Login button from menu'
WebUI.click(findTestObject('Common Test Cases/span_Login'))

'Verify "Login Button" should be disabled'
WebUI.waitForElementAttributeValue(findTestObject('Login Page/button_Login'), 'class', 'button active disabled', 3)

'Enter email address (input Excel file)'
WebUI.setText(findTestObject('Login Page/input_login_username'), findTestData('Login Page').getValue(2, 2))

'Enter password (input Excel file)'
WebUI.setText(findTestObject('Login Page/input_login_password'), findTestData('Login Page').getValue(2, 3))

'press TAB on keyboard\r\n'
WebUI.sendKeys(findTestObject('Login Page/input_login_password'), Keys.chord(Keys.TAB), FailureHandling.STOP_ON_FAILURE)

'Verify "Login Button" should be enabled'
WebUI.waitForElementAttributeValue(findTestObject('Login Page/button_Login'), 'class', 'button active', 3)

'click Login button'
WebUI.click(findTestObject('Login Page/button_Login'))

'click on Logo to go Home Page'
WebUI.click(findTestObject('Common Test Cases/Primary_Logo'))

