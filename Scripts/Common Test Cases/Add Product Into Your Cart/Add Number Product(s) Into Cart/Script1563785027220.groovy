import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

'IF USER ON JUNIOR'
if (WebUI.verifyElementPresent(findTestObject('Header/button_SiteOption_junior'), 1, FailureHandling.OPTIONAL)) {
    urlcurrent = WebUI.getUrl()

    URL netUrl = new URL(urlcurrent)

    hostUrl = netUrl.getHost()

    int a = 0

    int b = 17

    int c = Integer.parseInt(findTestData('Checkout Function').getValue(2, 1))

    while (a != c) {
        WebUI.navigateToUrl(hostUrl + findTestData('Checkout Function').getValue(2, b))

        if (WebUI.verifyElementPresent(findTestObject('Mini Cart/button_Size'), 1, FailureHandling.OPTIONAL) == true) {
            WebUI.click(findTestObject('Mini Cart/button_Size'))

            WebUI.waitForJQueryLoad(5)
        }
        
        //WebUI.delay(3)
        if (WebUI.verifyElementPresent(findTestObject('Mini Cart/button_AddToBag'), 1, FailureHandling.OPTIONAL) == true) {
            WebUI.click(findTestObject('Mini Cart/button_AddToBag'))
        } else {
            if (c < 5) {
                c = (c + 1)
            }
        }
        
        b = (b + 1)

        a = (a + 1 //WebUI.delay(3)
        )
    }
} else {
    urlcurrent = WebUI.getUrl()

    URL netUrl = new URL(urlcurrent)

    hostUrl = netUrl.getHost()

    int a = 0

    int b = 7

    int c = Integer.parseInt(findTestData('Checkout Function').getValue(2, 1))

    while (a != c) {
        WebUI.navigateToUrl(hostUrl + findTestData('Checkout Function').getValue(2, b))

        if (WebUI.verifyElementPresent(findTestObject('Mini Cart/button_Size'), 1, FailureHandling.OPTIONAL) == true) {
            WebUI.click(findTestObject('Mini Cart/button_Size'))

            WebUI.waitForJQueryLoad(5)
        }
        
        if (WebUI.verifyElementPresent(findTestObject('Mini Cart/button_AddToBag'), 1, FailureHandling.OPTIONAL) == true) {
            WebUI.click(findTestObject('Mini Cart/button_AddToBag'))
        } else {
            if (c < 5) {
                c = (c + 1)
            }
        }
        
        b = (b + 1)

        a = (a + 1)
    }
}

