import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

'IF USER ON JUNIOR'
if (WebUI.verifyElementPresent(findTestObject('Header/button_SiteOption_junior'), 3, FailureHandling.OPTIONAL)) {
    urlcurrent = WebUI.getUrl()

    URL netUrl = new URL(urlcurrent)

    hostUrl = netUrl.getHost()

    int jb = 16

    int jc = Integer.parseInt(findTestData('Checkout Function').getValue(2, 2).substring(8))

    int ja = jb + jc

    WebUI.navigateToUrl(hostUrl + findTestData('Checkout Function').getValue(2, ja))

    while (WebUI.verifyElementNotPresent(findTestObject('Mini Cart/button_Size'), 1, FailureHandling.OPTIONAL) == true) {
        if (jc < 5) {
            jc = (jc + 1)

            ja = (jc + jb)

            WebUI.navigateToUrl(hostUrl + findTestData('Checkout Function').getValue(2, ja))
        }
        
        if (jc == 5) {
            ja = (jb + 1)

            WebUI.navigateToUrl(hostUrl + findTestData('Checkout Function').getValue(2, ja))
        }
    }
    
    WebUI.click(findTestObject('Mini Cart/button_Size'))

    WebUI.waitForJQueryLoad(5)

    WebUI.click(findTestObject('Mini Cart/button_AddToBag'))
} else {
    urlcurrent = WebUI.getUrl()

    URL netUrl = new URL(urlcurrent)

    hostUrl = netUrl.getHost()

    int b = 6

    int c = Integer.parseInt(findTestData('Checkout Function').getValue(2, 2).substring(8))

    int a = b + c

    println(c)

    WebUI.navigateToUrl(hostUrl + findTestData('Checkout Function').getValue(2, a))

    println(a)

    while (WebUI.verifyElementNotPresent(findTestObject('Mini Cart/button_Size'), 1, FailureHandling.OPTIONAL) == true) {
        if (c < 5) {
            c = (c + 1)

            a = (c + b)

            WebUI.navigateToUrl(hostUrl + findTestData('Checkout Function').getValue(2, a))
        }
        
        if (c == 5) {
            a = (b + 1)

            WebUI.navigateToUrl(hostUrl + findTestData('Checkout Function').getValue(2, a))
        }
    }
    
    WebUI.click(findTestObject('Mini Cart/button_Size'))

    WebUI.waitForJQueryLoad(5)

    WebUI.click(findTestObject('Mini Cart/button_AddToBag'))
}

