<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Wishlist - active</name>
   <tag></tag>
   <elementGuidId>80e55c5c-caff-4556-9304-a39a79b8e3d7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;product-content&quot;]//following::a[contains(@class,&quot;active&quot;)]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;product-content&quot;]//following::a[contains(@class,&quot;active&quot;)]</value>
   </webElementProperties>
</WebElementEntity>
