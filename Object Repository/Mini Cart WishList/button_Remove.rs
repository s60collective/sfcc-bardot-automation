<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Remove</name>
   <tag></tag>
   <elementGuidId>d48e9257-6656-41d8-bcb2-331ef12fe88d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class=&quot;cart-table wishlist-table&quot;]/form[1]//following::button[contains(@name,&quot;dwfrm_wishlist_items_i&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@class=&quot;cart-table wishlist-table&quot;]/form[1]//following::button[contains(@name,&quot;dwfrm_wishlist_items_i&quot;)]</value>
   </webElementProperties>
</WebElementEntity>
