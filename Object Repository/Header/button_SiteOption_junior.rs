<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_SiteOption_junior</name>
   <tag></tag>
   <elementGuidId>4c137916-d774-42f2-bf2b-9510ea6f8527</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//div[@class='site-options']//following::li[@class=&quot;actived&quot;]/a[.='Bardot Junior']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class='site-options']//following::li[@class=&quot;actived&quot;]/a[.='Bardot Junior']</value>
   </webElementProperties>
</WebElementEntity>
