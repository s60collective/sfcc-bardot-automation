<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_Colour</name>
   <tag></tag>
   <elementGuidId>eae2da72-e69a-4cee-b656-4c768c879b76</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='cart-table']/div[${count}+1]//following::div[@data-attribute=&quot;color&quot;]/span[@class=&quot;value&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
