<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_Size</name>
   <tag></tag>
   <elementGuidId>d35f258b-eeee-4db0-9f79-ac8df80ee3a8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='cart-table']/div[${count}+1]//following::div[@data-attribute=&quot;size&quot;]/span[@class=&quot;value&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
