<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_Qty</name>
   <tag></tag>
   <elementGuidId>72a1039f-58a3-439e-9591-545f2ec7a232</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@id='cart-table']/div[${count}+1]//following::div[@class=&quot;item-quantity clearfix&quot;]/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@id='cart-table']/div[${count}+1]//following::div[@class=&quot;item-quantity clearfix&quot;]/input</value>
   </webElementProperties>
</WebElementEntity>
