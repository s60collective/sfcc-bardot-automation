<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_ProductName</name>
   <tag></tag>
   <elementGuidId>bbb05fe6-6828-4ead-9bbb-b266ab729dc4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='cart-table']/div[${count}+1]//following::div[@class=&quot;name&quot;]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
