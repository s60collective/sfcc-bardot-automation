<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_SalePrice</name>
   <tag></tag>
   <elementGuidId>02341615-01f5-4ec4-bbe6-851f8a0f4057</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@id='cart-table']/div[${count}+1]//following::div[@class=&quot;price-item desktop-only&quot;]//following::span[contains(@class,&quot;price-sales&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@id='cart-table']/div[${count}+1]//following::div[@class=&quot;price-item desktop-only&quot;]//following::span[contains(@class,&quot;price-sales&quot;)]</value>
   </webElementProperties>
</WebElementEntity>
