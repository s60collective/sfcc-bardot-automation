<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_Promotion</name>
   <tag></tag>
   <elementGuidId>e31552bd-67a1-4fa6-ac89-f915cf148549</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@id='cart-table']/div[${count}+1]//following::div[@class=&quot;attribute promo-section clearfix&quot;]/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@id='cart-table']/div[${count}+1]//following::div[@class=&quot;attribute promo-section clearfix&quot;]/span</value>
   </webElementProperties>
</WebElementEntity>
