<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Image_banner - Junior</name>
   <tag></tag>
   <elementGuidId>3ee2d474-8738-4bbb-8a13-ec3d02877934</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class=&quot;level-2&quot;]//following::img[contains(@src,'${data}')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class=&quot;level-2&quot;]//following::img[contains(@src,'${data}')]</value>
   </webElementProperties>
</WebElementEntity>
