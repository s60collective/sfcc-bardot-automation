<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Banner_button_URL</name>
   <tag></tag>
   <elementGuidId>c1f634ee-8aa9-4a3d-ae06-805f12cf6dca</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class=&quot; subcat_from_left&quot;]/div[@class=&quot;level-2&quot;]//following::a[contains(@href,'${dataa}')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@class=&quot; subcat_from_left&quot;]/div[@class=&quot;level-2&quot;]//following::a[contains(@href,'${dataa}')]</value>
   </webElementProperties>
</WebElementEntity>
