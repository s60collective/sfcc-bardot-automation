<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Banner_button_URL - Junior</name>
   <tag></tag>
   <elementGuidId>b4d5b674-5765-4961-97da-792d874b3060</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class=&quot;level-2&quot;]//following::a[contains(@href,'${dataa}')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class=&quot;level-2&quot;]//following::a[contains(@href,'${dataa}')]</value>
   </webElementProperties>
</WebElementEntity>
