<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>list_MegaMenu</name>
   <tag></tag>
   <elementGuidId>8e2a5451-69fe-44e1-87eb-f8f1258ca91b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'menu-category level-1']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//ul[@class='']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>menu-category level-1</value>
   </webElementProperties>
</WebElementEntity>
