<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>list_Sub_categories_column - Junior</name>
   <tag></tag>
   <elementGuidId>1d6d2201-61a6-4ef9-b4c3-e132d8ed5ebd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;navigation&quot;]/ul/li[${jncol}]//following::ul[@class=&quot;menu-columns have-banner&quot;]/li[@class=&quot;columns-${column}&quot;]/a</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;navigation&quot;]/ul/li[${jncol}]//following::ul[@class=&quot;menu-columns have-banner&quot;]/li[@class=&quot;columns-${column}&quot;]/a</value>
   </webElementProperties>
</WebElementEntity>
