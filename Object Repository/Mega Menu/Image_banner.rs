<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Image_banner</name>
   <tag></tag>
   <elementGuidId>55ea0399-748c-4a94-9f78-4b58823e8efd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class=&quot; subcat_from_left&quot;]/div[@class=&quot;level-2&quot;]//following::img[contains(@src,'${data}')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@class=&quot; subcat_from_left&quot;]/div[@class=&quot;level-2&quot;]//following::img[contains(@src,'${data}')]</value>
   </webElementProperties>
</WebElementEntity>
