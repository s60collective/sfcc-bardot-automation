<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>list_Sub_categories- Junior</name>
   <tag></tag>
   <elementGuidId>8e15c8bf-a150-4f3c-9cfe-85dd9c9c9679</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;navigation&quot;]/ul/li[${jncol}]//following::ul[@class=&quot;menu-columns have-banner&quot;]/li[@class=&quot;columns-${column}&quot;]//following::ul[@class=&quot;category-level3-col&quot;]/li[${jnrow2}]/a</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;navigation&quot;]/ul/li[${jncol}]//following::ul[@class=&quot;menu-columns have-banner&quot;]/li[@class=&quot;columns-${column}&quot;]//following::ul[@class=&quot;category-level3-col&quot;]/li[${jnrow2}]/a</value>
   </webElementProperties>
</WebElementEntity>
