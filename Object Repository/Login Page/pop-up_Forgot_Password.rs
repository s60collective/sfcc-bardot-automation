<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>pop-up_Forgot_Password</name>
   <tag></tag>
   <elementGuidId>38011b3c-9653-43c6-8d6d-2bc14f1dae0a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'PasswordResetForm']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>PasswordResetForm</value>
   </webElementProperties>
</WebElementEntity>
