<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_error_Email</name>
   <tag></tag>
   <elementGuidId>62ce8fef-a719-46b8-932d-f1b322ef7798</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[@class='error'][count(. | //*[starts-with(@id, 'dwfrm_login_username')]) = count(//*[starts-with(@id, 'dwfrm_login_username')])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[@class='error']</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>starts with</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>dwfrm_login_username</value>
   </webElementProperties>
</WebElementEntity>
