<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_login_password</name>
   <tag></tag>
   <elementGuidId>c5911caf-bbda-401d-889b-5303260b187b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[starts-with(@id, 'dwfrm_login_password')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>starts with</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>dwfrm_login_password</value>
   </webElementProperties>
</WebElementEntity>
