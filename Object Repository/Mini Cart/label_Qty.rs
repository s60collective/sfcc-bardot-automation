<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_Qty</name>
   <tag></tag>
   <elementGuidId>a3dd6c64-989e-4343-8e10-124c2f33b01f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class=&quot;simplebar-content&quot;]/div[${elecount}]//following::div[@class=&quot;attribute quantity&quot;]/span[@class=&quot;value&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class=&quot;simplebar-content&quot;]/div[${elecount}]//following::div[@class=&quot;attribute quantity&quot;]/span[@class=&quot;value&quot;]</value>
   </webElementProperties>
</WebElementEntity>
