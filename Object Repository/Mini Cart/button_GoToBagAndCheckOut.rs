<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_GoToBagAndCheckOut</name>
   <tag></tag>
   <elementGuidId>c5ef13c5-6248-4040-bc2b-8be2633ef332</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;mini-cart&quot;]//following::a[@class=&quot;button mini-cart-link-cart&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;mini-cart&quot;]//following::a[@class=&quot;button mini-cart-link-cart&quot;]</value>
   </webElementProperties>
</WebElementEntity>
