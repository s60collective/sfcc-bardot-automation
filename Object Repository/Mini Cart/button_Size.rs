<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Size</name>
   <tag></tag>
   <elementGuidId>fa9ccbe0-9915-4a29-95be-2fedea21bca9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;product-content&quot;]//following::ul[@class=&quot;swatches size&quot;]//following::li[contains(@class,'selectable')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;product-content&quot;]//following::ul[@class=&quot;swatches size&quot;]//following::li[contains(@class,'selectable')]</value>
   </webElementProperties>
</WebElementEntity>
