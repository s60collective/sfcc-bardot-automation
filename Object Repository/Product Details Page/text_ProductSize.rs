<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_ProductSize</name>
   <tag></tag>
   <elementGuidId>6cd19a61-4bf5-4993-83cd-09d684e70dde</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;product-content&quot;]//following::ul[@class=&quot;swatches size&quot;]//following::li[contains(@class,'selectable')]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;product-content&quot;]//following::ul[@class=&quot;swatches size&quot;]//following::li[contains(@class,'selectable')]/a</value>
   </webElementProperties>
</WebElementEntity>
