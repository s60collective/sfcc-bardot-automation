<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_ProductPrice</name>
   <tag></tag>
   <elementGuidId>29376f68-dc5b-4c3c-9562-f350cf3748a6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@id=&quot;product-content&quot;]//following::div[@class=&quot;product-price&quot;]/input[@name=&quot;salespriceValue&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@id=&quot;product-content&quot;]//following::div[@class=&quot;product-price&quot;]/input[@name=&quot;salespriceValue&quot;]</value>
   </webElementProperties>
</WebElementEntity>
